//
//  ViewController.h
//  TestProject
//
//  Created by Abraham Ventura on 6/12/12.
//  Copyright (c) 2012 Prolific Methods LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *length;

@property (weak, nonatomic) IBOutlet UITextField *diameterLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalWeight;



- (IBAction)chooseTreeButton:(id)sender;
- (IBAction)Calculate:(id)sender;

@end
