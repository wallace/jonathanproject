//
//  main.m
//  TestProject
//
//  Created by Abraham Ventura on 6/12/12.
//  Copyright (c) 2012 Prolific Methods LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
